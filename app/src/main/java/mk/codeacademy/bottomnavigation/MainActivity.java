package mk.codeacademy.bottomnavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        toolbar.setTitle("Shop");

        Log.d("TEST", "TEST");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.navigation_shop:
                toolbar.setTitle("Shop");
                return true;
            case R.id.navigation_gifts:
                toolbar.setTitle("My Gifts");
                return true;
            case R.id.navigation_cart:
                toolbar.setTitle("Cart");
                return true;
            case R.id.navigation_profile:
                toolbar.setTitle("Profile");
                return true;
        }
        return false;
    }
}
